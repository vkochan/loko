Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Loko Scheme
Upstream-Contact: Gwen Weinholt
Source: https://scheme.fail/
Comment:
  Public domain files are marked as MIT here. They were previously
  CC0-1.0 as recommended by REUSE, but people have opinions about
  that license.

Files:     Akku.manifest
           Akku.lock
           */*/Akku.manifest
           */*/Akku.lock
           tests/acpi/*.aml
           tests/perf/*.csv
Copyright: 2022 Gwen Weinholt
License:   EUPL-1.2+

Files:     lib/font/*.bdf
           lib/font/font-*.sls
Copyright: none
License:   MIT
Comment:
  These bitmap fonts from Markus Kuhn are marked as being public domain.
  Bitmap fonts may not even be copyrightable.

Files:     lib/match.sls
Copyright: none
License:   MIT
Comment:
  This code is written by Alex Shinn and placed in the
  Public Domain.  All warranties are disclaimed.

Files:     unicode/UNIDATA/*
           unicode/unicode-char-cases.ss
           unicode/unicode-charinfo.ss
Copyright: 1991-2019 Unicode, Inc.
License:   Unicode-DFS-2016

Files:     tests/r6rs/*
Copyright: 2010-2014 PLT Design Inc.
License:   MIT
Comment:
  The license is (Apache-2.0 OR MIT), so there is a choice. Using the
  MIT license avoids having to provide a copy of Apache-2.0.
  The name of the copyright holder was found with some git archeology on
  <https://github.com/racket/r6rs>.
