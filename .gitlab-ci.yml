# SPDX-FileCopyrightText: 2022 Gwen Weinholt
#
# SPDX-License-Identifier: EUPL-1.2+

image: "akkuscm/akku"

variables:
  XDG_CACHE_HOME: "$CI_PROJECT_DIR/.cache"
  AKKU_CACHE_DIR: "$CI_PROJECT_DIR/.cache/akku"

cache:
  key: "$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG"
  paths:
    - .akku/src
    - .cache/akku

stages:
  - licence
  - build
  - build_image
  - build_netbsd
  - test

reuse:
  stage: licence
  image:
    name: registry.gitlab.com/weinholt/loko:reuse
    # TODO: Use this image instead when reuse is released with the fix for + in licences
    #name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

# Bootstrap from Chez Scheme
dist:
  stage: build
  interruptible: true
  before_script:
    - apk add --no-cache make tar
  script:
    - make dist
  artifacts:
    paths:
      - loko-*.tar.gz

# Build a bootable hard drive image. This also embeds SYSLINUX in the
# image, which is GPL'd.
dist_hdd_img:
  stage: build_image
  image: "akkuscm/akku:loko"
  interruptible: true
  variables:
    GIT_STRATEGY: none
  needs: ["dist"]
  before_script:
    - sed 's/^deb /deb-src /' < /etc/apt/sources.list >> /etc/apt/sources.list
    - apt update && apt -y --no-install-recommends install make syslinux syslinux-common fdisk
  script:
    - apt-get source -d syslinux
    - tar -xzf loko-*.tar.gz && cd loko-*/
    - make
    - make install
    - make distimg EMBED_SRC="$(echo $PWD/../loko-*.tar.gz $PWD/../syslinux*)"
    - mv loko-hdd-*.gz ..
  artifacts:
    paths:
      - loko-hdd-*.gz

# Bootstrap a NetBSD build from Chez Scheme
build_netbsd:
  stage: build_netbsd
  interruptible: true
  before_script:
    - apk add --no-cache make tar
  script:
    - akku install
    - make bootstrap DEFAULT_TARGET=netbsd GDB_AUTOLOAD_PATH=/usr/pkg/share/gdb/auto-load
    - mv loko-prebuilt loko.netbsd-amd64
  artifacts:
    paths:
      - loko.netbsd-amd64

# Self-compilation for Linux/amd64
test_linux:
  stage: test
  image: "debian:buster"
  interruptible: true
  variables:
    GIT_STRATEGY: none
  needs: ["dist"]
  before_script:
    - apt update && apt -y --no-install-recommends install make
  script:
    - tar -xzf loko-*.tar.gz && rm loko-*.tar.gz && cd loko-*
    - make
    - make install
    - .akku/env loko --compile tests/test-buddy.sps --output tests/test-buddy
    - tests/test-buddy
  artifacts:
    paths:
      - loko.linux-amd64

# # Self-cross-compilation for NetBSD/amd64
# test_netbsd:
#   stage: test
#   interruptible: true
#   image: "debian:buster-slim"
#   variables:
#     GIT_STRATEGY: none
#   needs: ["dist"]
#   before_script:
#     - apt update && apt -y --no-install-recommends install make
#   script:
#     - tar -xzf loko-*.tar.gz && rm loko-*.tar.gz && cd loko-*
#     - make config.sls DEFAULT_TARGET=netbsd GDB_AUTOLOAD_PATH=/usr/pkg/share/gdb/auto-load
#     - make loko DEFAULT_TARGET=netbsd
#     - mv loko ../loko.netbsd-amd64
#   artifacts:
#     paths:
#       - loko.netbsd-amd64

# Bootstrap from Chez Scheme and then rebootstrap from Loko
test_rebootstrap:
  stage: test
  interruptible: true
  needs: []
  before_script:
    - apk add --no-cache make tar
  script:
    - make
    - make rebootstrap

test_built:
  stage: test
  interruptible: true
  needs: ["dist"]
  before_script:
    - apk add --no-cache make mtools sfdisk
    - akku update
  script:
    - akku install
    - (tar -xzf loko-*.tar.gz && rm loko-*.tar.gz && cd loko-* && make && make install)
    - .akku/env loko --program tests/test-fibers.sps
    - .akku/env loko --compile tests/test-buddy.sps --output tests/test-buddy
    - tests/test-buddy
    - .akku/env loko --program tests/test-drivers-ps2-keyboard.sps
    - (cd tests/r6rs; akku install; .akku/env loko --program run.sps)
    - make install
    - (cd tests/r6rs; akku install; .akku/env loko -feval --compile run.sps --output run.bin)
    - tests/r6rs/run.bin
    - (cd tests/r6rs; akku install; .akku/env loko -feval --compile run-via-eval.sps --output run-via-eval.bin)
    - tests/r6rs/run-via-eval.bin
    - .akku/env loko --program tests/test-unicode.sps
    - loko --script tests/r7rs/r7rs-tests.scm
    - (cd tests/r7rs; loko --program include-order.sps)
    - (cd bin/pc-repl; make)
    - loko --program tests/srfi-19-test-suite.scm
    - loko --program tests/test-n2s.sps
    - loko --program tests/test-bugs.sps
    - loko --program tests/test-eof.sps | tee tests/test-eof-result.txt && diff -u tests/test-eof-expect.txt tests/test-eof-result.txt
    - loko --compile tests/test-eof.sps && tests/test-eof | tee tests/test-eof-result.txt && diff -u tests/test-eof-expect.txt tests/test-eof-result.txt
    - loko --program tests/srfi/test-srfi-229.sps
    - loko --compile tests/srfi/test-srfi-229.sps && tests/srfi/test-srfi-229

# Check that the samples can be build
test_samples:
  stage: test
  interruptible: true
  needs: []
  before_script:
    - apk add --no-cache make
    - akku update
  script:
    - make
    - make install
    - make -j$(nproc) samples
    - samples/hello/hello
